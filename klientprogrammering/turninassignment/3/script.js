window.addEventListener('load', () => {

    async function updateFortune(withApiCall) {
        

        let resp = await fetch(withApiCall)
        let json = await resp.json()
        
        let fortune = document.getElementById('fortunedisplay')
        let div = document.createElement('div')
        if (fortune.children.length < 10) {
            div.innerText = json.fortune
            fortune.appendChild(div)
        } else {
            fortune.removeChild(fortune.lastChild)
            div.innerText = json.fortune
            fortune.prepend(div)
        }
       
        
        fortune.style.visibility = 'visible';
    }

    let randomFortuneBtn = document.getElementById('randomBtn');
    randomFortuneBtn.addEventListener('click', () => updateFortune('/api/fortune'))

    let shortFortuneBtn = document.getElementById('shortBtn');
    shortFortuneBtn.addEventListener('click', () => updateFortune('/api/short'))


    let showerFortuneBtn = document.getElementById('showerBtn')
    showerFortuneBtn.addEventListener('click', () => updateFortune('/api/showerthought'));


    let traditionalFortuneBtn = document.getElementById('traditionalBtn');
    traditionalFortuneBtn.addEventListener('click', () => updateFortune('/api/traditional'));
})



