# Min sida om amazonas

## Page1.html

Är en sida som visar lite brödtext och lite länkar kring olika djur i amazonen.
Främst apor...

Den har också länkar där man kan gå till andra sidor som pratar om djuren. Du kan också fylla i ett formulär om vad du gillar för djur och att du är medveten om att apor är coola.

Det finns även en länk till Page2.html

## Page2.html

Detta är en sida som visar en bild på en apa.

## Styles.css

Här har jag lagt det mesta av koden för att stilsätta sidan. Bland annat har jag använt mig av en flexbox för att lägga elementen av sidan i en ordning.
