//Task 1
function sumOfList(list) {

    let sum = 0;    
    for(let i = 0; i < list.length; i++){
           sum+= list[i];     
        }
    return sum;
}

function betterSumOfList(list) {

    let sum = 0;    // OF , nice to use! :D 
    for(const val of list){
           sum+= val;     
        }
    return sum;
}

function lambdaSumOfList(list) {
    return list.reduce((v1, v2) => v1 + v2)
}


//Task 2
function makeStringList2(numlist) {
    let stringList = [];
    for (const val of numlist) {
        stringList.push(String(val));
    }
    return stringList;
}

//Same as above but with mapfunction of list.
let numList = [1,2,3,4,5,6,7,8,9],
    stringList = numList.map(x => String(x));


