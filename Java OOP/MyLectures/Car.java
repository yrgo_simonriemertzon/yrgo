public class Car extends Object {
    int numberOfWheels;
    int maxDriveRanges;
    boolean hasFuelTank;
    boolean hasTurbo;
    String factoryName;

    public Car() {
        this.hasFuelTank = true;
        this.hasTurbo = false;
    }

    public Car(int numberOfWheels, int maxDriveRanges) {
        this();
        this.numberOfWheels = numberOfWheels;
        this.maxDriveRanges = maxDriveRanges;
    }

    public Car(int numberOfWheels, int maxDriveRanges, boolean hasTurbo) {
        this(numberOfWheels, maxDriveRanges);
       
    }

    public Car(int numberOfWheels, int maxDriveRanges, boolean hasTurbo, String factoryName) {
        this(numberOfWheels, maxDriveRanges, hasTurbo);
        this.factoryName = factoryName;
    }

    @Override
    public String toString() {
        return "Car [factoryName=" + factoryName + ", hasFuelTank=" + hasFuelTank + ", hasTurbo=" + hasTurbo
                + ", maxDriveRanges=" + maxDriveRanges + ", numberOfWheels=" + numberOfWheels + "]";
    }


}
