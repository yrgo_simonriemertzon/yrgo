/**A simple model of book reviews */
public class Rating implements Comparable<Rating> {
    private String reviewerAlias;
    private String email;
    private int rating;

    /** A simple method for getting the reviewer e-mail
     * @return email address entered (could be without @)
     */
    public String getEmail() {
        return this.email;
    }

    /** A simple method for getting the book rating
     * @return rating must be 1-10
     */
    public int getRating() {
        return this.rating;
    }

    /**A simple constructor
     * @param reviewerAlias Name of the reviewer
     * @param email Email belonging to the reviewer (could be without @)
     * @param rating Reviewers rating of the book.
     */
    public Rating(String reviewerAlias, String email, int rating) {
        this.reviewerAlias = reviewerAlias;
        this.email = email;
        this.rating = rating;
    }

    /**Shows all the member variables
     * @return [reviewer name], [email], [rating]
    */
    @Override
    public String toString() {
        return String.format("%s, %s, %d", reviewerAlias, email, rating);
    }

    /**Simple comparing the email of the reviewers
     * @param r Rating from a user with email and score
     * @return the value 0 if the rating email is equal to the other email;
     *  a value less than 0 if the rating email is lexicographically less than the other email; 
     *  and a value greater than 0 if the rating email is lexicographically greater than other email.
     */
    @Override
    public int compareTo(Rating r) {
        return getEmail().compareTo(r.getEmail());
    }

}
