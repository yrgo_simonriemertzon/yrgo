import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**A simple program to enter ratings of books using the terminal. */
public class StepFour {

    /**main method that adds 3 books and ask the user for inputs
     * @param args not used
    */
    public static void main(String[] args) {
        List<Book> books = new ArrayList<>(List.of(new Book("Nisse Hult", "En bondes bekännelser", 1121223121L),
                new Book("Lars Lerin", "Konstig konst", 13223434343L),
                new Book("Jane Smith", "The big book", 98392839238L)));

        try (Scanner scanner = new Scanner(System.in);) {
            while (true) {
                showAvailable(books);
                System.out.println("Vilken bok vill du betygsätta? (skriv avsluta för att avsluta)");
                boolean isNumber = scanner.hasNextInt();
                var answer = scanner.nextLine().trim();
                if (answer.toLowerCase().equals("avsluta")) {
                    break;
                }

                if (isNumber) {
                    int chosenBook = Integer.parseInt(answer);
                    if (books.size() == 0 || chosenBook < 1 || chosenBook > books.size()-1) {
                        System.err.println("Den bokens finns inte. Prova på nytt.");
                        continue;
                    }
                    giveRating(books, scanner, chosenBook);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void giveRating(List<Book> books, Scanner scanner, int chosenBook) {
        System.out.print("Vad heter du? ");
        System.out.flush();
        var alias = scanner.nextLine();
        System.out.flush();
        System.out.print("Vilken epost har du? ");
        var email = scanner.nextLine();
        System.out.flush();
        int chosenRating;
        do {
            System.out.print("Vad är ditt betyg (1-10)? ");
            chosenRating = scanner.nextInt();
            if (chosenRating >= 1 && chosenRating <= 10) {
                break;
            }
        } while (true);
        
        scanner.nextLine();
        var rating = new Rating(alias, email, chosenRating);
        books.get(chosenBook - 1).addRating(rating);
    }

    private static void showAvailable(List<Book> books) {
        System.out.println("Vi har följande böcker:");
        var counter = 1;
        Collections.sort(books, new BookComparator().reversed() );
        for (Book book : books) {
            System.out.println(counter + ". " + book);
            counter++;
        }
    }

}
