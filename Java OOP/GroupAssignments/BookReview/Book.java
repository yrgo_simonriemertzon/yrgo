import java.util.ArrayList;
import java.util.List;

/**A model of a book available for sale. */
public class Book {
    private String title;
    private String author;
    private long ISBN;
    private List<Rating> ratings;
    private double averageRating;

    /**A simple method for getting book average rating
     * @return Average rating for the book from reviewers.
     */
    public double getAverageRating() {
        return this.averageRating;
    }

    /**A simple constructor that also sets averageRating to -1 for not used and creates ratings list
     * @param author Book author with first and last name
     * @param title Book title in full
     * @param ISBN Book ISBN-number used for sorting
     */
    public Book(String author, String title, long ISBN) {
        this.title = title;
        this.author = author;
        this.ISBN = ISBN;
        this.averageRating = -1;
        ratings = new ArrayList<>();
    }

    /**Adds a new rating for a book and also calculates the new rating average
     * @param r Rating of the book with reviewer alias, email and rating (1-10)
     */
    public void addRating(Rating r) {
        for (Rating rating : ratings) {
            if (rating.compareTo(r) == 0) {
                throw new IllegalArgumentException("Can't review twice");
            }
        }
        ratings.add(r);
        calculateAverage();
    }

    /**Shows all the member variables
     * @return title - author ISBN averageRating/(No rating available)
    */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("%s - %s %d ", title, author, ISBN));
        if (ratings.size() == 0) {
            sb.append("No rating available");
        } else {
            sb.append(String.format("%.1f", this.averageRating));
        }
        return sb.toString();
    }

    private void calculateAverage() {
        double average = 0.0;
        for (Rating rating : ratings) {
            average += rating.getRating();
        }
        average = average / ratings.size();
        this.averageRating = average;
    }



}