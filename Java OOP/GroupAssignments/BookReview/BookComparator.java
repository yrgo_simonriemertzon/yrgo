import java.util.Comparator;

/**A simple comparator that compares average ratings of books */
public class BookComparator implements Comparator<Book> {

    /**Compares two books which has higher average rating
     * @param o1 first book to be compared
     * @param o2 second book to be compared
     * @return the value 0 if o1 average value is numerically equal to o2;
     *  a value less than 0 if o1 has average rating less than o2; 
     * and a value greater than 0 if o1 has greater average rating than o2.
     */
    @Override
    public int compare(Book o1, Book o2) {
        return Double.compare(o1.getAverageRating(), o2.getAverageRating());
    }
}
