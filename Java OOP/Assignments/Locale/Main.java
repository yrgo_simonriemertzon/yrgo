import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Locale locale = Locale.forLanguageTag("en-GB-x-oed");

        try (Scanner scanner = new Scanner(System.in)) {
            scanner.useLocale(locale);
            double[] numbers = new double[3];
            System.out.println("Enter three decimal numbers");
            for (int i = 0; i < numbers.length; i++) {
                numbers[i] = scanner.nextDouble();
            }
            
            double sum = Arrays.stream(numbers).sum();
            double average = sum / numbers.length;
            String output = String.format(locale, "The average of the numbers is %.3f", average);
            System.out.println(output);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}