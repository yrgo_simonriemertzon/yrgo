import java.util.Scanner;

public class SimonRiemertzonInl2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int input = scan.readInt();
        int sumOdd = 0;
        int sumEven = 0;
        for (int i = 1; i <= input; i++) {
            if (i % 2 == 0) {
                sumEven += i;
            } else {
                sumOdd += i;
            }
        }
        System.out.printf("Summan av udda tal: $s/n", sumOdd);
        System.out.printf("Summan av jämna tal: $s/n", sumEven);

    }
}