import java.util.Scanner;

public class TaskTwo {

    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(System.in);
        System.out.println("Choose a height in meters");
        int heightInMeters = scan.nextInt();
        double heightInCentimeters = heightInMeters * 100;
    
        int counter = 0;
        while(heightInCentimeters > 1) {
            heightInCentimeters *= 0.7;
            counter++;
            
        }
        System.out.println(counter);
        
    }
   

    



}