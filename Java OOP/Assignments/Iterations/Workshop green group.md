## How much information can we store in one bit:
1 and 0

## Examples of one bit information: 
- OFF / ON
- TRUE / FALSE
- NORD / SYD
- ÖST / VÄST

## This is because 2^1 is two. 

## Number of comibnations in n bits
How many combinations are there in 10 bits
2^10 = 1024 combinations = 0000 0000 00 , 0101 0101 01 and so on.

## How many bits do we need to store one million numbers? 
2^20 = 1,048,576

## A ball park figure for 2^32? 
GOOGLE IT!

2^2 * 2^2 = 2^4

# Binary Numbers - converting to decimal
64 32 16 8 4 2 1

         1 0 0 0 = 8 
         1 1 1 1 = 15
         1 1 1 0 = 14
         1 1 0 1 = 13
    1  1 1 0 0 1 = 57 (- 32 = 25 - 16 = 9 - 8 = 1)

0123456789       : Bas 10
01               : Bas 2
0123456789ABCDEF : Bas 16 = HEX

                    Dec   Hex
ASCI Tabellen : A = 64     41


# Binary numbers, convert from decimal
(Unsigned)
128 64 32 16 8 4 2 1
________________
           0 1 0 1 = 5
           0 0 1 1 = 3  
           0 1 1 1 = 7
           0 1 1 0 = 6
           0 0 0 0 = 0
           0 0 1 0 = 2





128 64 32 16 8 4 2 1
____________________                
1          1  1 1 1   
1   1  1  1  1 1 1 1
        +
0   0  0  0  0 0 1 1  
0          0  0 0 1 0       

Signed system:
128 64 32 16 8 4 2 1
____________________                
0   0   0  0 1 1 1 1  = 
         +
0   0   0  0 0 0 0 1  = 1
        =
        0  1 0 0 0 0 = 8 positive






















128 64 32 16 8 4 2 1
(Signed)
________________
0  0   0  0 0 1 0 1 = 5
1  1   1  1 1 0 1 0 = -5
           