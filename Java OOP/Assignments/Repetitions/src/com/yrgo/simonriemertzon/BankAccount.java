package com.yrgo.simonriemertzon;

public abstract class BankAccount {
    private static String ACCOUNTID = "";
    private double totalValue;

    public BankAccount(String accountID) {
        this.ACCOUNTID = accountID;
        this.totalValue = 0.0;
    }

    public double getTotalValue() {
        return totalValue;
    }
    public String getAccountID() {
        return ACCOUNTID;
    }

    public boolean deposit(double amount) {
        if(amount < 0) {
            throw new IllegalArgumentException("Amount cant be negative");
        }
        totalValue += amount;
        return true;
    }

    public boolean withdraw(double amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("Can't withdraw negative amount");
        }

        if (totalValue < amount) {
            return false;
        }

        totalValue -= amount;
        return true;
    }


}
