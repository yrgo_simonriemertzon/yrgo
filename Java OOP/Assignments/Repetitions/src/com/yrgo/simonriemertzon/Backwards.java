package com.yrgo.simonriemertzon;

import java.io.*;
import java.nio.Buffer;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Backwards {
    public static void main(String[] args) throws IOException {
        Path textFile = FileSystems.getDefault().getPath("apa.txt");
        validateInputFile(textFile);

        List<String> lines = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(textFile.toFile()))) {
            String line;
            while ((line = reader.readLine()) != null) {
                lines.add(line);
            }
        }
        System.out.println(mostOf(lines, 'e'));




/*        try {
            reverseFile(textFile);
        } catch (FileNotFoundException e) {
            System.err.println("File not found.");
            System.err.println(e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }


    private static String mostOf(List<String> textLines, char ch) {
        String biggestTextLine = "";
        long charsFound = 0;
        for (String line : textLines) {
            long count = line.chars().filter(c -> c == 'e').count();
            if (charsFound < count) {
                charsFound = count;
                biggestTextLine = line;
            }
        }


/*        for (int i = 0; i < textLines.length; i++) {
            var count = 0;
            String line = textLines[i];
            for (int j = 0; j < line.length(); j++) {
                if (line.charAt(j) == ch) {
                    count++;
                }
                if (charsFound < count) {
                    charsFound = count;
                    biggestTextLine = line;
                }
            }
        }*/
        return biggestTextLine;
    }

    private static void validateInputFile(Path textFile) throws IOException {
        if (!Files.exists(textFile)) {
            System.err.println("No such file exist.");
            System.exit(-1);
        }

        try {
            if (Files.size(textFile) > 1024 * 1024) {
                System.err.println("Input file cannot be over 1MB.");
            }
        } catch (IOException e) {
            System.err.println("Could not read file-size of file: " + textFile.toAbsolutePath());
            System.exit(-1);
        }
    }

    public static void reverseFile(Path filePath) throws IOException {
        StringBuilder toBeWritten = readFile(filePath);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("output.txt"))) {
            writer.write(toBeWritten.reverse().toString().trim());
        }
    }

    public static StringBuilder readFile(Path filePath) throws IOException {
        StringBuilder sb = new StringBuilder();

        try (BufferedReader reader = new BufferedReader(new FileReader(filePath.toFile()))) {
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line.trim());
                sb.append("\n");
            }
        }
        return sb;
    }
}
