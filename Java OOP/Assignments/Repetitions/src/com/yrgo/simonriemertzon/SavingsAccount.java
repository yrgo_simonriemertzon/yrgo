package com.yrgo.simonriemertzon;

public class SavingsAccount extends BankAccount{
    private double interest;

    public SavingsAccount(String accountID, double interest) {
        super(accountID);
        this.interest = interest;
    }

    public void addInterestToAccount() {
        var interestValue = getTotalValue() * interest;
        deposit(interestValue);
    }
}
