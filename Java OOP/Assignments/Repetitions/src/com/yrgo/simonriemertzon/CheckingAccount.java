package com.yrgo.simonriemertzon;

public class CheckingAccount extends BankAccount {
    private double fee = 10;

    public CheckingAccount(String accountID, double fee) {
        super(accountID);
        this.fee = fee;
    }

    public void payFee(double fee) {
        if(!withdraw(fee)){
            throw new RuntimeException();
        }

    }



}
