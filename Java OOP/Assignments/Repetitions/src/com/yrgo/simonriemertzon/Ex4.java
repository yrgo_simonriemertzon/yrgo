package com.yrgo.simonriemertzon;

import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

public class Ex4 {
    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("Du måste ange två filnamn.");
            System.exit(-1);
        }
        String inputFile = args[0];
        String outputFile = args[1];
        try {
            Path inputPath = FileSystems.getDefault().getPath(inputFile);
            if (Files.size(inputPath) > 1024 * 1024) {
                System.out.println("Infilen kan vara max 1MB stor.");
                System.exit(-1);
            }
        } catch (IOException ex) {
            System.out.println("Kunde inte ta reda på filens storlek: " +
                    ex.getMessage());
            System.exit(-1);
        }
        StringBuilder builder = new StringBuilder();
        char[] buffer = new char[4096];
        try (BufferedReader reader = new BufferedReader(new
                FileReader(inputFile))) {
            while (true) {
                int size = reader.read(buffer);
                if (size == -1) {
                    break;
                }
                builder.append(buffer, 0, size);
            }
        } catch (IOException ex) {
            System.out.println("Fel under inläsning av fil: " + ex.getMessage());
            System.exit(-1);
        }

        try (BufferedWriter writer = new BufferedWriter(new
                FileWriter(outputFile))) {
            writer.write(builder.reverse().toString());
        } catch (IOException ex) {
            System.out.println("Fel under skrivning av fil: " + ex.getMessage());
            System.exit(-1);
        }
    }
}