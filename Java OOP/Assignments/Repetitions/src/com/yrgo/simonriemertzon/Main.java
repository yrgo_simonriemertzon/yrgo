package com.yrgo.simonriemertzon;

import java.util.HashMap;

public class Main {

    public static void main(String[] args) {
        var savingsAccount = new SavingsAccount("1203-3323-9000-1111", 0.03);
        var checkingsAccount = new CheckingAccount("1203-3323-9000-2222", 20);

        savingsAccount.deposit(1150);
        savingsAccount.withdraw(100);
        savingsAccount.addInterestToAccount();

        checkingsAccount.deposit(1100);
        checkingsAccount.withdraw(100);
        checkingsAccount.payFee(20);
        
        printAccountDetails(savingsAccount);
        printAccountDetails(checkingsAccount);


    }

    public static void printAccountDetails(BankAccount account) {
        System.out.printf("Account (%s) has %.2f in it ", account.getAccountID(), account.getTotalValue());
    }
}
