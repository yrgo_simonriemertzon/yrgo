package com.yrgo.simonriemertzon;

public interface GameDie {

    int roll();
    String getDescription();


}
