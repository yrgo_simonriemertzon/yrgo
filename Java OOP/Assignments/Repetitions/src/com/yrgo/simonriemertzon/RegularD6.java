package com.yrgo.simonriemertzon;

import java.util.concurrent.ThreadLocalRandom;

public class RegularD6 implements GameDie {

    @Override
    public int roll() {
        return ThreadLocalRandom.current().nextInt(0, 6) + 1;
    }

    @Override
    public String getDescription() {
        return "tärningen";
    }
}
