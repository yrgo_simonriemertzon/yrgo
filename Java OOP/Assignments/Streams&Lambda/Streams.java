import java.util.ArrayList;
import java.util.Scanner;
import java.util.function.Predicate;

public class Streams {
    public static void main(String[] args) {
        var s = new Scanner(System.in);
        var list = new ArrayList<String>();
        System.out.println("Please input names, end each name with a newline");    
        while (true) {
            var name = s.nextLine();
            if(name.equals("")) {
                break;
            }
            list.add(name);
            
        }
    
        String someString = "Hello world";
        
        list.stream()
                    .filter(Predicate.not(String::isBlank))
                    .map(s -> s.replaceAll("\\s", ""))
                    .forEach(String::replaceAll("\\s", "")
                    .map(String::replaceAll("\\s", ""))
                    .forEach(System.out::println);

    }
}
