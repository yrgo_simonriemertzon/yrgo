import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Ex1 {
    private static List<String> names = new ArrayList<>();
    public static void main(String[] args) {
        
        try(Scanner s = new Scanner(System.in)) {
            while (true) {
                String line = s.nextLine();
                if (line.isEmpty()){break;}
                names.add(line);                
            }
            

        }

        for (String name : names){
            System.out.println(name);
        }

        
    }
}
