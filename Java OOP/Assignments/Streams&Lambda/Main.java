import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        var s = new Scanner(System.in);
        var list = new ArrayList<String>();
        System.out.println("Please input names, end each name with a newline"); 
        try(Scanner scan = new Scanner(System.in)) {

            while (true) {
                var name = s.nextLine();
                if(name.trim().isEmpty() {
                    break;
                }
                list.add(name);
                
            }
        
        }   
        for (String name : list) {
            System.out.println(name);
        }

    }
}