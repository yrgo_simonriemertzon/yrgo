import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Ex2 {
    private static List<String> names = new ArrayList<>();
    public static void main(String[] args) {
        
        try(Scanner s = new Scanner(System.in)) {
            while (true) {
                String line = s.nextLine();
                if (line.isEmpty()){break;}
                names.add(line);                
            }
            

        }
        
        names.stream()
        .map(String::trim)
        .map(String::toLowerCase)
        .forEach(System.out::println);

        
    }

    private static Object replaceAll(String string, String string2) {
        return null;
    }
}
