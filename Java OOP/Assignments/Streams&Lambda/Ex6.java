import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Ex6 {
    private static List<String> names = new ArrayList<>();
    public static void main(String[] args) {
        
        try(Scanner s = new Scanner(System.in)) {
            while (true) {
                String line = s.nextLine();
                if (line.isEmpty()){break;}
                names.add(line);                
            }
            

        }
        
        List<String> filteredNames = names.stream()
        .map(s -> s.replaceAll("\\s", ""))
        .filter(s -> s.length() > 2)
        .map(String::toLowerCase)
        .collect(Collectors.toList());

        for (String name : filteredNames) {
            System.out.println(name);
        }

        
    }

    private static Object replaceAll(String string, String string2) {
        return null;
    }
}
