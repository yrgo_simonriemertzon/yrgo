import java.util.Objects;

public class Pair<T1, T2> {
    private T1 value1;
    private T2 value2;

    public T1 getValue1() {
        return this.value1;
    }

    public void setValue1(T1 value1) {
        this.value1 = value1;
    }

    public T2 getValue2() {
        return this.value2;
    }

    public void setValue2(T2 value2) {
        this.value2 = value2;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if(!(obj instanceof Pair)) {
            return false;
        }

        var possiblePair = (Pair) obj;

        return Objects.equals(value1, possiblePair.value1)
        && Objects.equals(value2, possiblePair.value2);
        
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((value1 == null) ? 0 : value1.hashCode());
        result = prime * result + ((value2 == null) ? 0 : value2.hashCode());
        return result;
        
    
    }

}
