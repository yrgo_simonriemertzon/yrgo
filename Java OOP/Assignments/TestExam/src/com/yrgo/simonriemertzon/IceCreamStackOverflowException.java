package com.yrgo.simonriemertzon;

public class IceCreamStackOverflowException extends RuntimeException {
    public static final long serialVersionUID = 1345657364;

    public IceCreamStackOverflowException() {

    }

    public IceCreamStackOverflowException(String message) {
        super(message);
    }

    public IceCreamStackOverflowException(String message, Throwable cause) {
        super(message, cause);
    }
}
