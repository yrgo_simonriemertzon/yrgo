package com.yrgo.simonriemertzon;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class App {
    public static void main(String[] args) {
        IceCream myIceCream = new IceCream(IceCream.Holder.CUP);

        try {
            myIceCream.addScoop(new IceCreamScoop("Vanilla", 5));
            myIceCream.addScoop(new IceCreamScoop("Ice", 5));
            myIceCream.addScoop(new IceCreamScoop("Ice", 5));
            myIceCream.addScoop(new IceCreamScoop("Baby", 5));

        } catch (IceCreamStackOverflowException e) {
            System.out.println(e.getMessage());
        }
        System.out.println(myIceCream);

        List<Computer> computers = Arrays.asList(
         new Computer("MacBookPro", 3.4, 8000, 512),
        new Computer("Hp Elitebook", 4, 16000, 250),
        new Computer("Hp Elitebook", 4, 8000, 250),
        new Computer("Chromebook", 2.6, 8000, 1024));

        Collections.sort(computers, Collections.reverseOrder());
        for (Computer computer: computers
             ) {
            System.out.println(computer);
        }


    }
}

