package com.yrgo.simonriemertzon;

import java.util.Objects;

public class IceCreamScoop {
    private String flavour;
    private double weight;

    public IceCreamScoop(String flavour, double weight) {
        this.flavour = flavour;
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "IceCreamScoop{" +
                "flavour='" + flavour + '\'' +
                ", weight=" + weight +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IceCreamScoop that = (IceCreamScoop) o;
        return Objects.equals(flavour, that.flavour);
    }

    @Override
    public int hashCode() {
        return Objects.hash(flavour);
    }

    public String getFlavour() {
        return flavour;
    }


}
