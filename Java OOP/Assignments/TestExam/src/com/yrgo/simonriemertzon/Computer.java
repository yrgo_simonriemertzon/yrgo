package com.yrgo.simonriemertzon;

public final class Computer implements Comparable<Computer>{
    private final String name;
    private final double cpuSpeed;
    private final int ramSize;
    private final int hdSize;

    public Computer(String name, double cpuSpeed, int ramSize, int hdSize) {
        this.name = name;
        this.cpuSpeed = cpuSpeed;
        this.ramSize = ramSize;
        this.hdSize = hdSize;
    }

    public String getName() {
        return name;
    }

    public double getCpuSpeed() {
        return cpuSpeed;
    }

    public int getRamSize() {
        return ramSize;
    }

    public int getHdSize() {
        return hdSize;
    }

    @Override
    public int compareTo(Computer o) {
        int resultCpuSpeed = Double.compare(cpuSpeed, o.cpuSpeed);
        if(resultCpuSpeed == 0) {
            int resultRamsize = Integer.compare(ramSize, o.ramSize);
            if(resultRamsize == 0) {
                return Integer.compare(hdSize, o.hdSize);
            } else {
                return resultRamsize;
            }
        }
        return resultCpuSpeed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Computer computer = (Computer) o;

        if (Double.compare(computer.getCpuSpeed(), getCpuSpeed()) != 0) return false;
        if (getRamSize() != computer.getRamSize()) return false;
        if (getHdSize() != computer.getHdSize()) return false;
        return getName().equals(computer.getName());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + name.hashCode();
        result = prime * result + Double.valueOf(ramSize).hashCode();
        result = prime * result + Integer.valueOf(hdSize).hashCode();
        return result;
    }

    @Override
    public String toString() {
        return String.format("%s - %.2fghz, %dRAM, %dmb HD", name, cpuSpeed, ramSize, hdSize);
    }
}
