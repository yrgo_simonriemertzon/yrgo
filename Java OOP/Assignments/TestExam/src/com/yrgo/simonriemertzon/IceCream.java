package com.yrgo.simonriemertzon;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class IceCream {

    public enum Holder {
        CONE, CUP
    }

    private List<IceCreamScoop> scoops;
    private Holder holder;
    private static final int MAXIMUMSCOOPS = 5;

    public IceCream(Holder holder) {
        this.holder = holder;
        scoops = new ArrayList<>();
    }



    public void addScoop(IceCreamScoop scoop) throws IceCreamStackOverflowException {
        if (scoops.size() < MAXIMUMSCOOPS) {
            scoops.add(scoop);
        } else {
            String message = "A, a, aaah! Cant have more than 5 scoops or the " + getHolder().toString().toLowerCase() +
                    " will overflow!";
            throw new IceCreamStackOverflowException(message);
        }
    }

    public int size() {
        return scoops.size();
    }

    public Holder getHolder() {
        return holder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IceCream iceCream = (IceCream) o;

        if (!scoops.equals(iceCream.scoops)) return false;
        return holder == iceCream.holder;
    }

    @Override
    public int hashCode() {
        int result = scoops.hashCode();
        result = 31 * result + holder.hashCode();
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Icecream in a " + holder.toString().toLowerCase() + " with the flavours ");
        for (int i = 0; i < scoops.size(); i++) {
            IceCreamScoop scoop = scoops.get(i);

            if (!(i == scoops.size() - 1)) {
                sb.append(scoop.getFlavour() + ", ");
            } else {
                sb.append("and " + scoop.getFlavour() + ".");
            }
        }
        return sb.toString();
    }
}
