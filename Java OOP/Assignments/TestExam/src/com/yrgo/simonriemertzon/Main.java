package com.yrgo.simonriemertzon;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import java.util.HashMap;
public class Main {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.err.println("You must supply a input textfile");
            System.exit(-1);
        }
        if(args.length == 1) {
            System.err.println("You must name a output file");
            System.exit(-1);
        }

        Path pathToInFile = validateInputFile(args[0]);
        HashMap<String, Integer> uniqueWords = new HashMap<>();
        fillHashMapWith(uniqueWords, pathToInFile);

        StringBuilder sb = buildString(pathToInFile, uniqueWords);
        writeOutputFile(args[1], sb);
    }

    private static void writeOutputFile(String outputFile, StringBuilder sb) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile))) {
            bw.write(sb.toString());
        } catch (FileNotFoundException e) {
            System.err.println("File could not be created");
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }

    private static StringBuilder buildString(Path pathToInFile, HashMap<String, Integer> uniqueWords) {
        StringBuilder sb = new StringBuilder();
        LocalDateTime dt = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        sb.append(String.format("Filename: %s %n", pathToInFile));
        sb.append(String.format("Date: %s %n", formatter.format(dt)));
        sb.append(String.format("Words processed: %d %n", uniqueWords.size()));
        var numberOfUniqueWords = uniqueWords.entrySet().stream()
                .filter(e -> e.getValue() < 2)
                .count();
        sb.append(String.format("Total unique words: %d %n", numberOfUniqueWords));
        return sb;
    }

    private static void fillHashMapWith(HashMap<String, Integer> uniqueWords, Path fromPathToInFile) {
        String line;

        try (BufferedReader br = new BufferedReader(new FileReader(fromPathToInFile.toString()))) {
            while ((line = br.readLine()) != null) {
                String[] splitWords = line.trim().toUpperCase().split("\\s");
                for (String word : splitWords) {
                    int value = uniqueWords.getOrDefault(word, 0);
                    uniqueWords.put(word, value + 1);
                }
                System.out.println(uniqueWords);
            }

        } catch (FileNotFoundException e) {
            System.err.printf("Given filename(%s) Cannot be opened for reading or is a directory. %n",
                    fromPathToInFile.getFileName().toString());
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Path validateInputFile(String commandLineArgument) {
        Path pathToInFile = null;
        try {
            pathToInFile = FileSystems.getDefault().getPath(commandLineArgument).toAbsolutePath();
            if (!Files.exists(pathToInFile)) {
               System.out.println("The file " + pathToInFile.toString() + " does not exist");
               System.exit(-1);
            }

        } catch (InvalidPathException e) {
            System.out.println("Filename could not be parsed" + e.getMessage());
            System.out.println(e.getStackTrace());
        }

        return pathToInFile;
    }
}
