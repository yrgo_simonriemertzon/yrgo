import java.io.IOException;

public class Frequency {
    public static void main(String[] args) {

        try {
            if (args.length == 0) {
                throw new NullPointerException();
            }

            FrequencyReader reader = new FrequencyReader(args);
            System.out.println(reader);

        } catch (IOException e) {
            System.err.println("Unknown IO error:" + e.getMessage());
        } catch (NullPointerException e) {
            System.err.println("Proper Usage is: java Frequency [OPTIONS] <filename>");
            System.exit(0);
        }
    }
}