public class Ex1 {
    private static List<String> names = new ArrayList<>();
    public static void main(String[] args) {
        
        try(Scanner s = new Scanner(System.in)) {
            while((line = s.nextLine()) != null) {
                names.add(line);
            }
        }
    }
}
