import java.util.Scanner;
/**
 * SimonRiemertzonInl2 is a class that nicely prints the result of the even and odd numbers from 1, 
 * to the given number. 
 * 
 * Example, given number 5:
 * Odd = 1 + 3 + 5 
 * Even = 2 + 4
 * Prints out: 9 and 6
 */
public class SimonRiemertzonInl2 {
        public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int input = scan.nextInt();
        int sumOdd = 0;
        int sumEven = 0;
        for (int i = 1; i <= input; i++) {
            if (i % 2 == 0) {
                sumEven += i;
            } else {
                sumOdd += i;
            }
        }
        System.out.printf("Summan av udda tal: " + sumOdd + "\n");
        System.out.printf("Summan av jämna tal: " + sumEven +"\n");
    }
}