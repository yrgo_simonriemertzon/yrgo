import java.util.Scanner;

public class TaskThree {
    public static void main(String[] args) {
       
        System.out.println("Skriv in ett tal");
        Scanner scan = new Scanner(System.in);
        
        System.out.println("Talet består av " + howManyDigitsIn(scan.nextDouble()) + " siffror");

    


    }

    private static int howManyDigitsIn(double number) {
        int counter = 0;
        while (number > 1) {
            number /= 10;
            counter++;
        }
        return counter;

    }

  


}