public class PrimeNumber {

    public void printOutPrimeNumbers(int amountsOfPrimeNumbers) {
        int counter = amountsOfPrimeNumbers;
        for (int i = 3; i <= Integer.MAX_VALUE && counter > 0; i++) {
                if (isPrime(i)) {
                    System.out.println(i);
                    counter--;
                }
            }
        
      
    }

    private static boolean isPrime(int number) {
        for (int i = 2; i <= number/2; i++) {
            if(number % i == 0) {
                return false;
            }
        }
        return true;
    }


}