public class Person implements Comparable<Person> {
    private String name;
    private String lastName;
    private String birthYear;

    public Person(String name, String lastName, String birthYear) {
        this.name = name;
        this.lastName = lastName;
        this.birthYear = birthYear;
    }

    public String getName() {
        return this.name;
    }

    public String getLastName() {
        return this.lastName;
    }


    public String getBirthYear() {
        return this.birthYear;
    }

    @Override
    public int compareTo(Person o) {
        return lastName.compareTo(o.lastName);
    }

    @Override
    public String toString() {
        return String.format("Firstname: %s Lastname: %s", name, lastName);
    }

}
