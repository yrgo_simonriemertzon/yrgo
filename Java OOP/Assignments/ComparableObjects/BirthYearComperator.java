import java.util.Comparator;

public class BirthYearComperator implements Comparator<Person> {

    @Override
    public int compare(Person o1, Person o2) {
        String name1 = o1.getBirthYear();
        String name2 = o2.getBirthYear();
        
        return name1.compareTo(name2);
    }

}
