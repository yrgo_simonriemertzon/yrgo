import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Comparator<Person> comparator = Comparator.comparing(Person::getBirthYear, Comparator.naturalOrder());

        Person person = new Person("Simon", "Riemertzon", "2020");
        Person person2 = new Person("Carolina", "Nyberg", "1991");
        Person person3 = new Person("Flo", "Carlsson", "2000");

        List<Person> persons = new ArrayList<Person>();
        persons.add(person);
        persons.add(person2);
        persons.add(person3);

        persons.sort(Comparator.comparing(Person::getBirthYear, Comparator.naturalOrder()));
        

        
        for(Person p : persons) {
            System.out.println(p);
        }




    }
}
