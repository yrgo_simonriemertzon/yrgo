import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        
        List<Person> people = new ArrayList<Person>();
        Person p1 = new Person("Simon", "Riemertzon", 1990);
        Person p2 = new Person("Bert", "Karlsson", 1987);
        Person p3 = new Person("Bosse", "Andersson", 1999);
        Person p4 = new Person("Bosse", "zendersson", 2000);

        people.add(p1);
        people.add(p2);
        people.add(p3);
        people.add(p4);

        Collections.sort(people);

        System.out.println(people.toString());




    }
}