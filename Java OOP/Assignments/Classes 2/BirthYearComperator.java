import java.util.Comparator;

public class BirthYearComperator implements Comparator<Person> {

    @Override
    public int compare(Person o1, Person o2) {
        if (o1.getBirthYear() < o2.getBirthYear()) {
            return -1;
        }
    }
    
}
