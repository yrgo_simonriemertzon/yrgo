public class Person implements Comparable<Person> {
    
    private String fname, ename; 
    private int birthYear;

    public Person(String fname, String ename, int birthYear) {
        this.fname = fname;
        this.ename = ename;
        this.birthYear = birthYear;
    }

    @Override
    public int compareTo(Person o) {
        return ename.compareTo(o.ename);
    }
    
    public int getBirthYear() {
        return birthYear;
    }
    
    public String getFname() {
        return fname;
    }
    
    public Object getEname() {
        return this.ename;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s", fname, ename, birthYear);
    }
    

}
