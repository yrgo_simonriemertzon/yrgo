public class Mean implements Operation {

    @Override
    public String getName() {
        return "mean";
    }

    @Override
    public int operation(int[] values) {
        int sum = 0;
        for (int value : values) {
            sum += value;
        }
        return sum / values.length;
    }
    
}
