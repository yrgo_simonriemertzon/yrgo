public class Minimum implements Operation {
    @Override
    public String getName() {
       return "min";
    }

    @Override
    public int operation(int[] values) {
        int minimum = values[0];
        for (int i = 1; i < values.length; i++) {
            minimum = minimum > values[i] ? values[i] : minimum;
        }
        return minimum;
    }

}
