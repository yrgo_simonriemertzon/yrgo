public class Measurement {
    int value;
    String unit;

    public Measurement(int value, String unit) {
        this.value = value;
        this.unit = unit;
    }

    public boolean isNegative() {
        return value < 0;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    
    @Override
    public String toString() {
        return  value + unit;        
    }


    
    

}
