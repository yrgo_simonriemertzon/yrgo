import java.io.IOException;
import java.rmi.server.Operation;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(System.in);
        Operation[] ops = {new Minimum(), new Maximum()};
        int[] values = new int[4];

        for (int i = 0; i < values.length; i++) {
            System.out.println("Enter a number");
            values[i] = scan.nextInt();
        }
        
        System.out.println("What would you like to calculate? min/max");
        for (Operation op : ops) {
            System.out.println("- " + op.getName());
        }
    
        int result = 0;
        boolean validChoice = false;
        String choice = scan.next();

        for (Operation op : ops) {
            if(op.getName().equals(choice)) {
                result = op.operation(values);
                validChoice = true;
                break;
            }
        }

        if (validChoice) {
            System.out.println("The answer is: " + result);
        }
        

    }

    

}