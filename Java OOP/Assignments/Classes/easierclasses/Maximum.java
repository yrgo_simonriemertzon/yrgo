public class Maximum implements Operation {
    @Override
    public String getName() {
        return "max";
    }

    @Override
    public int operation(int[] values) {
        int maximum = values[0];
        for (int i = 1; i < values.length; i++) {
            maximum = maximum < values[i] ? values[i] : maximum;
            ;
        }
        return maximum;
    }
}
