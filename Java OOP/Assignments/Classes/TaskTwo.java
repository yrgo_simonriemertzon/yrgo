/**
 * TaskTwo is a class that prints out a car if it is a classic car.
 */

public class TaskTwo {
    /**
     * Here in main we execute the car code
     * @param args
     */
    public static void main(String[] args) {
        Car carOne = new Car("ABC123", "Ford", 1990, 2000, 150);
        Car carTwo = new Car("DEF456", "Toyota", 2019, 1500, 300);
    
        if (carOne.isClassic()) System.out.println(carOne); else System.out.println("The car isn't a classic");

        System.out.printf("%B%n", carOne.isClassic());
        System.out.printf("%B%n", carTwo.isClassic());

    }
}
