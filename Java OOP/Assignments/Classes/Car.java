public class Car {
    private String regNumber;
    private String model;
    private int year;
    private double weight;
    private int power;

    /**
     * Here we set the cars properties to their default values. 
     * @param regNumber
     * @param model
     * @param year
     * @param weight
     * @param power
     */
    public Car(String regNumber, String model, int year, double weight, int power) {
        this.regNumber = regNumber;
        this.model = model;
        this.year = year;
        this.weight = weight;
        this.power = power;
    }

    /**
     * @return a boolean describing whether a car is classic or not. 
     */
    public Boolean isClassic() {
        if(2020 - year >= 25) return true; else return false;
    }
    /**
     * Returns a string representation of the car. 
     */
    public String toString() {
        return "Regnumber: " +  regNumber + "\nModel: " + model + "\nYear: " +   year + "\nWeight: " + weight + "\nPower: " + power;
    }





























    public String getRegNumber() {
        return this.regNumber;
    }

    public void setRegNumber(String regNumber) {
        this.regNumber = regNumber;
    }

    public String getModel() {
        return this.model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return this.year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getWeight() {
        return this.weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getPower() {
        return this.power;
    }

    public void setPower(int power) {
        this.power = power;
    }


    
}
