public class Triangle extends Shape {
    int height;
    int width;

    public Triangle(int height, int width) {
        super("Triangle");
        this.height = height;
        this.width = width;
    }

    @Override
    public String toString() {
        return "Height: " + height + " Width: " + width + "Name: " + super.toString();
    }




    
}
