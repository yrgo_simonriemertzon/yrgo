public class Rectangle {
    private double height = 0;
    private double width = 0;

    public Rectangle() {
        this(1, 1);
    }
    
    public Rectangle(double height, double width) {
        this.height = height;
        this.width = width;
    }

    public String toString() {
    return "Height: " + height + "\n"
            +  "Width: " + width + "\n";
    }

}
