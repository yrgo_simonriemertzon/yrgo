public class TaskTwo {
    
    public Symbol getType(String oneLetter) {
        String vowels = "aeiou";
        String consonants = "bcdfghjklmnpqrstvzx";
    
        if(vowels.contains(oneLetter)){
            return Symbol.VOWEL;
        }
        else if(consonants.contains(oneLetter)){
            return Symbol.CONSONANT;
        }
        else return Symbol.OTHER;
    }
}



/*    /*Övning 2
Skriv en metod som returnerar om en given bokstav är en vokal, konsonant eller ett annat tecken. Använd en
uppräkning (enum) för att beskriva de tre olika returvärdena.
Skriv sedan en metod som räknar antalet vokaler, konsonanter och andra tecken i en given textsträng och
skriver ut det på skärmen.
Skriv även ett huvudprogram där användaren får mata in en textsträng och får reda på ovan information om
strängen. */