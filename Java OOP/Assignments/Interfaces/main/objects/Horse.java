package main.objects;

import main.Player;
import main.interfaces.Interactive;

public class Horse implements Interactive {

    @Override
    public void interact(Player player) {
        player.setMaximumSpeed(player.getMaximumSpeed() + 10);
        System.out.println("Yiiiha!");
    }

    
}
