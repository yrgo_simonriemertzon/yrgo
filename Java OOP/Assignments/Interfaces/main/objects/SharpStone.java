package main.objects;
import main.Player;
import main.interfaces.Interactive;

public class SharpStone implements Interactive {

    @Override
    public void interact(Player player) {
        player.takesDamage(5);       
    }
    

}
