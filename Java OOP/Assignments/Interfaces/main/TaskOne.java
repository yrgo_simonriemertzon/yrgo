package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import main.objects.*;
import main.interfaces.Interactive;

public class TaskOne {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        List<Interactive> interactivles = new ArrayList<>();
        interactivles.add(new SharpStone());
        interactivles.add(new Horse());
        Player p1 = new Player("Bosse");


        System.out.println("Du ser: en vass sten, en häst");
        String input;
        do {
            System.out.println("Vad vill du använda?");
            input = scan.nextLine();
            if (input.toLowerCase().equals("en vass sten")) {
                stone.interact(p1);
            }
            
            if (input.toLowerCase().equals("en häst")) {
                horse.interact(p1);
            }


        } while (!input.equals("avsluta"));

        System.out.println("Ha en bra dag!");

    }


}