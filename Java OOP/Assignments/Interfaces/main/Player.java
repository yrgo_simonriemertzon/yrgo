package main;

public class Player {
    private String name;
    private int hitpoints;
    private int maximumSpeed;

    public int getMaximumSpeed() {
        return this.maximumSpeed;
    }

    public void setMaximumSpeed(int maximumSpeed) {
        this.maximumSpeed = maximumSpeed;
    };
    
    public Player(String name) {
        this.name = name;
        this.hitpoints = 10;
        this.maximumSpeed = 10;
    }

    public String getName() {
        return this.name;
    }
    
    public int getHitpoints() {
        return this.hitpoints;
    }

    




    public void takesDamage(int amount) {
        this.hitpoints =- amount;
        System.out.printf("Ouch! %s took %d damage%n", this.name, amount);     
    }

    
    

}
