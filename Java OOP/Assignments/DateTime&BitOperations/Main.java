import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Main {

    public static void main(String[] args) {
        System.out.println(Task1.doTask());
        System.out.println(Task2.doTask());
        Task3.doTask();
        Task4.doTask();
        Task5.doTask();
    }
}