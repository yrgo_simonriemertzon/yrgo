import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class Task3 {
    private static LocalDateTime today = LocalDateTime.now();
    private static DateTimeFormatter outputFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private static LocalDateTime inAMonth = today.plusMonths(1);

    private static ZoneId gothenburg = ZoneId.of("Europe/Stockholm");
    private static ZoneId newYork = ZoneId.of("America/New_York");

    public static void doTask() {
        DateTimeFormatter hoursAndMinutes = DateTimeFormatter.ofPattern("HH:mm:ss");    
        System.out.println(ZonedDateTime.now(gothenburg).format(hoursAndMinutes));
        System.out.println(ZonedDateTime.now(newYork).format(hoursAndMinutes));

    }

}
