import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Task2 {
    private static LocalDateTime today = LocalDateTime.now();
    private static DateTimeFormatter outputFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private static LocalDateTime inAMonth = today.plusMonths(1);
    

    public static String doTask() {
       return inAMonth.format(outputFormat);
    }
    
}
