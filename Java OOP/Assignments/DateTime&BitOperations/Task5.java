import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

public class Task5 {
    

    
    public static void doTask() {
        LocalDate date = null;
        try (Scanner scan = new Scanner(System.in);) {
            while (true) {
                
                System.out.println("Write in your birthdate yyyy-mm-dd");
                String dateString = scan.nextLine();
    
                try {
                    date = LocalDate.parse(dateString, DateTimeFormatter.ISO_DATE);
                    break;
                } catch (DateTimeParseException e) {
                    System.out.println("Not a valid date format " + e.getMessage()); 
                }
            }

            long days = ChronoUnit.DAYS.between(date, LocalDate.now());
            System.out.printf("You are %d days old. %n", days);


        } catch (Exception e) {
            //TODO: handle exception
        }
        
        

        
        
    }
}
