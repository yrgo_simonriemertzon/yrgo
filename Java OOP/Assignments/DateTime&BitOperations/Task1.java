import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Task1 {
    private static LocalDateTime today = LocalDateTime.now();
    private static DateTimeFormatter outputFormat = DateTimeFormatter.ISO_DATE;
    
    public static String doTask() {
       return today.format(outputFormat);
    }
    

}
