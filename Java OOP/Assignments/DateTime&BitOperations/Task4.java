import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class Task4 {
    private static LocalDateTime today = LocalDateTime.now();
    private static DateTimeFormatter outputFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private static LocalDateTime newYear = LocalDateTime.of(today.getYear() + 1, 1, 1, 0, 0, 0);
    private static Duration toNewYear = Duration.between(today, newYear);

    public static void doTask() {
        System.out.println(toNewYear.toDays());
    }

}
