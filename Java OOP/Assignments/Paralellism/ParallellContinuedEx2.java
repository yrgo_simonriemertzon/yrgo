import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ParallellContinuedEx2 {
    public static void main(String[] args) {
        ExecutorService service = Executors.newCachedThreadPool();

        for (int i = 0; i < 5; i++) {
            service.submit(() -> {
                for (int j = 0; j < 50; j++) {
                    System.out.println("j" + j );
                }
            });
        }
        service.shutdown();
    }
}
