import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.TreeMap;

/**
 * The class FrequencyReader is used to read frequencys of characters. The
 * characters are read from a filepath supplied to the constructor.
 * 
 * @author Simon Riemertzon
 */

public class FrequencyReader {

    private Map<Character, Integer> frequencyMap = new TreeMap<>();
    private Boolean ignoreCase;

    /**
     * If the first argument is -i we ignore case of read characters. This is saved
     * in the instance variable ignoreCase.
     * 
     * @param args
     * @throws IOException
     */
    public FrequencyReader(String[] args) throws IOException {
        ignoreCase = args[0].equals("-i") ? true : false;
        createFrequencyMapFrom(args);
    }

    /**
     * Loop through the files and for each line look for every unique character.
     * Save to a key in frequencyMap and set value to 1. 
     * For every equal character bump up value of correspoding key by 1. 
     * 
     * @param args paths to the files to be read
     * @throws IOException
     */
    private void createFrequencyMapFrom(String[] args) throws IOException {
        int i = ignoreCase ? 1 : 0;
        FileReader fileReader;
        for (; i < args.length; i++) {

            fileReader = createFileReaderFrom(args[i]);
            if (fileReader == null) {
                System.out.println("Reading next file...");
                continue;
            }

            try (BufferedReader reader = new BufferedReader(fileReader)) {
                String line;

                while ((line = reader.readLine()) != null) {

                    char[] characters = line.replaceAll("\\s", "").toCharArray();
                    for (char c : characters) {
                        if (ignoreCase) {
                            c = Character.toLowerCase(c);
                        }
                        if (!frequencyMap.containsKey(c)) {
                            frequencyMap.put(c, 1);
                        } else {
                            frequencyMap.replace(c, frequencyMap.get(c) + 1);
                        }
                    }
                }
            }
        }
    }
    /**
     * Create a new Filereader instance from path.
     * @param path to file
     * @return FileReader object
     * @throws FileNotFoundException
     */
    private FileReader createFileReaderFrom(String path) throws FileNotFoundException {
        Path file = FileSystems.getDefault().getPath(path);
        if (!Files.exists(file)) {
            System.err.println("No such file exists: " + path);
            return null;
        }
        FileReader reader = new FileReader(file.toFile());
        return reader;
    }
   
    @Override
    /**
     * Print out frequencyMap in a nice way.
     */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (var entry : frequencyMap.entrySet()) {
            sb.append(entry.getKey() + ": " + entry.getValue() + "\n");
        }

        return sb.toString();
    }
}
