import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Exercise3 {
    public static void main(String[] args) throws IOException {

        
        try (BufferedReader br = new BufferedReader(new FileReader("thisIsTheFile.txt"))) 
        {
            String s;
            while ((s = br.readLine()) != null) {
                System.out.println(s);
            }
        }
    }
}
        
        
         
    
        

