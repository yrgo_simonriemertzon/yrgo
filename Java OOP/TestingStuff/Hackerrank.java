import java.util.List;
import java.util.ArrayList;

public class Hackerrank {
    public static void main(String[] args) {

        List<Integer> a = new ArrayList<>(List.of(1, 2, 4));
        List<Integer> b = new ArrayList<>(List.of(1, 2, 3));

        System.out.println(score(a, b));

    }

    private static List<Integer> score(List<Integer> a, List<Integer> b) {
        List<Integer> result = new ArrayList<Integer>(List.of(0, 0));

        for (int i = 0; i < a.size(); i++) {
            int aValue = a.get(i);
            int bValue = b.get(i);

            if (aValue > bValue) {
                int currentScore = result.get(0);
                result.set(0, currentScore + 1);
            } else if (bValue > aValue) {

                int currentScore = result.get(1);
                result.set(1, currentScore + 1);
            }
            // No change to points if equal score.
        }

        return result;
    }
}