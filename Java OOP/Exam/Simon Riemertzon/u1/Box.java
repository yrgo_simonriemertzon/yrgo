import java.util.Objects;

public class Box implements Comparable<Box> {

    private double length;
    private double height;
    private double width;

    public Box(double length, double height, double width) {
        this.length = length;
        this.height = height;
        this.width = width;
    }

    public Box() {
        this.length = 1;
        this.height = 1;
        this.width = 1;
    }

    public double getLength() {
        return length;
    }

    public double getHeight() {
        return height;
    }

    public double getWidth() {
        return width;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }

        if(!(obj instanceof Box)) {
            return false;
        }

        Box box = (Box) obj;

        return Objects.equals(this.length, box.length) &&
                Objects.equals(this.width, box.width) &&
                Objects.equals(this.height, box.height);
       
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + Double.hashCode(length);
      result = prime * result + Double.hashCode(width);
      result = prime * result + Double.hashCode(height);
      return result;
    }

    @Override
    public int compareTo(Box o) {
        int resultComparedLengths = Double.compare(this.length, o.getLength());

        if (resultComparedLengths == 0) {
            int resultComparedWidth = Double.compare(this.width, o.getWidth());

            if (resultComparedWidth == 0) {
                return Double.compare(this.height, o.getHeight());
            } else {
                return resultComparedWidth;
            }
        }
        return resultComparedLengths;
    }

    @Override
    public String toString() {
        return String.format("%.2f %.2f %.2f ", length, width, height);
    }

}