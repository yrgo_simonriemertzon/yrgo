import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Main {

    public static void main(String[] args) {
        if (args.length == 0) {
            System.err.println("You must provide two textfiles");
            System.out.println("Usage: java Main <input file> <output file>");
            System.exit(-1);
        }

        Path[] inputAndOutput = validateArguments(args);

        Path inputFile = inputAndOutput[0];
        Path toOutPutFile = inputAndOutput[1];
        try {
            String[] result = (removeVowelsFrom(inputFile));
            createOutPutFileFrom(inputFile, toOutPutFile, result);

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private static String[] removeVowelsFrom(Path input) throws IOException {
        String line;

        StringBuilder sb = new StringBuilder();
        int numberOfVowelsRemoved = 0;
        try (BufferedReader reader = new BufferedReader(new FileReader(input.toString(), StandardCharsets.UTF_8))) {
            while ((line = reader.readLine()) != null) {
                // UGH... vill verkligen inte göra såhär
                
                
                for (int i = 0; i < line.length(); i++) {
                    if (line.charAt(i) == 'a' ||line.charAt(i) ==  'e' ||line.charAt(i) ==  'i' ||line.charAt(i) ==  'o' ||line.charAt(i) ==  'u' ||line.charAt(i) ==  'å' ||line.charAt(i) ==  'ä' || line.charAt(i) == 'ö') {  
                        numberOfVowelsRemoved++;
                    }
                }
                String s = line.toLowerCase().replaceAll("[aeiouåäö]", "");
                sb.append(String.format("%s%n", s ));

            }
        }
        System.out.println(sb.toString());
        String[] result = new String[] {sb.toString(), String.valueOf(numberOfVowelsRemoved)};
        return result;
    }

    private static void createOutPutFileFrom(Path toInFile, Path toOutFile, String[] result) {
        StringBuilder sb = new StringBuilder();
        LocalDateTime date = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        sb.append(String.format("Filename: %s %n", toInFile));
        sb.append(String.format("Date: %s %n", formatter.format(date)));
        sb.append(String.format("Vowels removed: %s %n%n", result[1]));
        sb.append(String.format(result[0]));

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(toOutFile.toString()))) {
            bw.write(sb.toString());
        } catch (FileNotFoundException e) {
            System.err.println("File could not be created");
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }

    private static Path[] validateArguments(String[] args) {

        Path input = FileSystems.getDefault().getPath(args[0]);
        Path output = FileSystems.getDefault().getPath(args[1]);

        if (!Files.exists(input)) {
            System.err.println("The input file does not exist");
            System.exit(-1);
        }

        if (Files.exists(output)) {
            System.err.println("The output file already created");
            System.out.println("Remove the output file and try again");
            System.exit(-1);
        }
        Path[] inputAndOutput = new Path[] { input, output };
        return inputAndOutput;

    }

}
