import java.util.Scanner;

public class AssignmentFour {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("How many points did you get on the test?  ");
        var points = scanner.nextInt();
        
        if (points >= 20 && points < 35) {
            System.out.println("That means you got a C");
        }
        else if (points >= 35 && points < 44) {
            System.out.println("That means you got an A");
        }
    }
}

