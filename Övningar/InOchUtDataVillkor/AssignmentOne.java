import java.util.Scanner;

public class AssignmentOne {
    public static void main(String[] args) {
        System.out.println("Welcome to rectanglemaker");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please assign a width in cm");
        double width = scanner.nextDouble();
        System.out.println("Please assign a height in cm");
        double height = scanner.nextDouble();
        
        System.out.printf("Circumferance %.2f cm\n", width * 2 + height * 2);
        System.out.printf("Area %.2f cm^2\n", width * height);
    
    }
}
