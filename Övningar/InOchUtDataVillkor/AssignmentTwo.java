import java.util.Scanner;

public class AssignmentTwo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please assign a width in cm");
        double width = scanner.nextDouble();
        System.out.println("Please assign a height in cm");
        double height = scanner.nextDouble();

        double circumferance =  width * 2 + height * 2;
        double area = width * height;
        
        System.out.printf("Circumferance %.2f cm\n", circumferance);
        System.out.printf("Area %.2f cm^2\n", area);
        
        if (area >= 1000) {
            System.out.println("Daaaaamn thats a big rectangle");
        }
    
    }
}
