import java.util.Scanner;

public class AssignmentSix {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Skriv in ett heltal");
        int firstNumber = scanner.nextInt();
        System.out.println("Skriv in ett nytt heltal");
        int secondNumber = scanner.nextInt();
        
        System.out.println("Vad vill du veta om talen? (min/max)");
        String answer = scanner.next();

    
        switch(answer.toLowerCase()) {
            case "max":
            int max = firstNumber > secondNumber ? firstNumber : secondNumber;
            System.out.println(max);
            break;

            case "min":
            int min = firstNumber < secondNumber ? firstNumber : secondNumber;
            System.out.println(min);
            break;
        }
    }
}
