import java.util.Scanner;

public class AssignmentFive {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int length;
        int width;
        int thickness;

        System.out.println("Assign letter length");
        length = scanner.nextInt();
        System.out.println("Assign letter thickness");
        thickness = scanner.nextInt();
        System.out.println("Assign letter width");
        width = scanner.nextInt();
        
        boolean underMaxLength = length < 600;
        boolean underMaxThickness = thickness < 100;
        boolean underMaxDimensions = (thickness + width + length) < 900;
        boolean overMinimumDimension = length >= 140 && width >= 90;

        if (underMaxDimensions && underMaxLength && underMaxThickness && overMinimumDimension) {
                System.out.println("Correct dimensions of letter");
        } else {
                System.out.println("Your letter is the wrong dimensions");
        }
    }
}

