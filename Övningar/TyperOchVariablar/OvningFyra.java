class OvningFyra {

    public static void main(String[] args) {
        double radius = 1e20f;

        double circumference = (float) (radius * 2 * StrictMath.PI);
        double area = (float) (Math.pow(radius, 2) * StrictMath.PI);

        System.out.printf("Omkretsen på cirkeln är %.2f.\n", circumference);
        System.out.printf("Arean på cirkeln är %.2f.\n", area);
        
    }

}