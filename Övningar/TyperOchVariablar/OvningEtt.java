public class OvningEtt {
    public static void main(String[] args) {

        byte oneByte = Byte.MAX_VALUE;
        short oneShort = Short.MAX_VALUE;
        int oneInteger = Integer.MAX_VALUE;
        long oneLong = Long.MAX_VALUE;
        float oneFloat = Float.MAX_VALUE;
        double oneDouble = Double.MAX_VALUE;
        boolean oneBoolean = true;
        char oneChar = 'A';

        System.out.println(
            "Byte: " + oneByte + "\n" 
            + "Short: " + oneShort + "\n"
            + "Integer: " + oneInteger + "\n"
            + "Long: " + oneLong + "\n" 
            + "Float: " + oneFloat + "\n" 
            + "Double: " + oneDouble + "\n" 
            + "Boolean: " + oneBoolean + "\n" 
            + "Char: " + oneChar);

    }

}