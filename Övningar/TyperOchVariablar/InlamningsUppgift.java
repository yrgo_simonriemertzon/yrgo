public class InlamningsUppgift {
    public static void main(String[] args) {
        byte aByte = 100;
        short aShort = 200;
        int anInt = 300;
        float aFloat = 500;
        double aDouble = 600;

        /* Jag har valt double som typ för mitt resultat, därför att:
            1. Double är stort:
            Med stor sannolikhet kommer jag kunna få plats med mer eller mindre
            allting som finns i de andra variablarna. 
            2. Double kan hantera flyttal. I en long hade jag kunnat få plats ännu mer,
            men då long inte hanterar flyttal/decimaltal så skulle data kunna gå förlorad.
            Dessutom skulle jag behöva typkonvertera deklareringen av result 
            (= aByte + aShort + anInt + aFloat + aDouble) vilket är onödigt skrivande.
         */
        double result = aByte + aShort + anInt + aFloat + aDouble;
        System.out.println(result);
    }
}
