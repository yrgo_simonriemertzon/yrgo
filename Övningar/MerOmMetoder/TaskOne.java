import java.util.Scanner;

public class TaskOne {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        boolean restartGame = false;
        do {
            GuessingGame game = new GuessingGame();
            System.out.println("Gissa vilket tal jag tänker på?");
            int guess = scan.nextInt();
            int result = game.guess(guess);
            if (result == 0) {
             System.out.println("Hurra, du lyckades!");
                                 System.out.println("Vill du spela igen?(J/N)");
                restartGame = scan.next().toUpperCase() == "J" ? true : false;
            }
            else if(result < 0) {
                System.out.println("Talet är mindre än " + guess);m
            } else {
                System.out.println("Talet jag söker är större än " + guess);
            }
            

        }while(restartGame!=true);

}

}