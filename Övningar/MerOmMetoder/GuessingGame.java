import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class GuessingGame {
    private int secretNumber;
    private int guessesMade;

    public GuessingGame() {
        this.secretNumber = ThreadLocalRandom.current().nextInt(10) + 1;
        guessesMade = 0;
    }

    /**
     * 
     * @param theGuess
     * @return 0 if theGuess and the secret number is the same, -1 if the guess is less, and 1 if it is greater.
     */
    public int guess(int theGuess) {
        guessesMade++;
        if(theGuess == secretNumber) {
            return 0;
        }
        else if(theGuess < secretNumber) {
            return -1;
        }
        else {
            return 1;
        }
    }

    public int getGuessesMade() {
        return this.guessesMade;
    }


}
