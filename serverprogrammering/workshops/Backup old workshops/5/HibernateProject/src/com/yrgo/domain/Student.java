package com.yrgo.domain;

import javax.persistence.*;
import javax.persistence.Table;
import javax.persistence.Column;

@Entity
@Table(name = "TBL_STUDENT")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

/*
    @ManyToOne
    @JoinColumn(name = "TUTOR_FK")
    private Tutor tutor;
*/

    @Column(name = "NUM_COURSES")
    private Integer numberOfCourses;

    private String enrollmentID;
    private String name;

    public Student() {
    }

    public Student(String name) {
        this.name = name;
        this.numberOfCourses = 10;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", numberOfCourses=" + numberOfCourses +
                ", enrollmentID='" + enrollmentID + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}

