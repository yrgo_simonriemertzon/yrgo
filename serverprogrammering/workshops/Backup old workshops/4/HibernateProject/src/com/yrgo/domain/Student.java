package com.yrgo.domain;

import javax.persistence.*;
import javax.persistence.Table;
import javax.persistence.Column;

@Entity
@Table(name = "TBL_STUDENT")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name = "TUTOR_FK")
    private Tutor tutor;

    @Column(name = "NUM_COURSES")
    private Integer numberOfCourses;

    private String enrollmentID;
    private String name;

    public Student() {
    }

    public Student(String name) {
        this.name = name;
        this.numberOfCourses = 10;
    }

    public Student(String name, Tutor tutor) {
        this.name = name;
        this.tutor = tutor;
        this.numberOfCourses = 10;
    }

    public void setTutor(Tutor tutor) {
        this.tutor = tutor;
    }


    public Tutor getTutor() {
        return tutor;
    }

    public void allocateTutor(Tutor tutor) {
        this.tutor = tutor;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", enrollmentID='" + enrollmentID + '\'' +
                ", name='" + name + '\'' +
                ", tutorName='" + getTutorName() + '\'' +
                '}';
    }

    public String getTutorName() {
        return this.tutor.getName();
    }
}

