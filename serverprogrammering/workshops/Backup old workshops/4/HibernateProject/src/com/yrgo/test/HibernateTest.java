package com.yrgo.test;

import com.yrgo.domain.Student;
import com.yrgo.domain.Tutor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class HibernateTest {
    private static SessionFactory sessionFactory = null;
    private static Session session;

    private static void initiateSession() {
        SessionFactory sf = getSessionFactory();
        session = sf.openSession();
    }

    private static void closeSession() {
        session.close();
    }

    public static void main(String[] args) {
        initiateSession();
        initiateDB();
        getStudent(1);
        getStudentAndTutorAndPrintThem();
        printTutorNameFromStudent(1);
        closeSession();
    }

    private static void printTutorNameFromStudent(int id) {
        Student myStudent = (Student) session.get(Student.class, id);
        Tutor myTutor = myStudent.getTutor(); // TODO: 15-Apr-21 This must not return null
        System.out.println("This is my tutors name: " + myTutor.getName());
    }

    private static void initiateDB() {
        Transaction tx = session.beginTransaction();
        Tutor newTutor = new Tutor("ABC 126 Beep beep I'm a Jeep", "Bosse");
        Student newStudent = new Student("Annabelle", newTutor);
        session.save(newStudent);
        session.save(newTutor);
        tx.commit();
    }

    private static void getStudent(int id) {
        Student myStudent = (Student) session.get(Student.class, id);
        System.out.println(myStudent + " is the student");
    }

    private static void getStudentAndTutorAndPrintThem() {
        SessionFactory sf = getSessionFactory();
        Session session = sf.openSession();
        Student myStudent = (Student) session.get(Student.class, 1);
        Tutor myTutor = (Tutor) session.get(Tutor.class, 1);
        System.out.println(myStudent);
        System.out.println(myTutor);
    }

    private static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            Configuration configuration = new Configuration();
            configuration.configure();
            ServiceRegistry serviceRegistry = new ServiceRegistryBuilder()
                    .applySettings(configuration.getProperties())
                    .buildServiceRegistry();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        }
        return sessionFactory;
    }
}
