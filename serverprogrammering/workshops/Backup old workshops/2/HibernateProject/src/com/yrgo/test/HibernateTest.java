package com.yrgo.test;

import com.yrgo.domain.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;


public class HibernateTest {
    private static SessionFactory sessionFactory = null;

    public static void main(String[] args) {
        addStudent();
        getStudent();
    }

    private static void addStudent() {
        SessionFactory sf = getSessionFactory();
        Session session = sf.openSession();
        Transaction tx = session.beginTransaction();

        Student newStudent = new Student("SimmePimme");
        session.save(newStudent);

        tx.commit();
        session.close();
    }

    private static void getStudent() {
        SessionFactory sf = getSessionFactory();
        Session session = sf.openSession();
        Student myStudent = (Student) session.get(Student.class, 2);
        System.out.println(myStudent + "is the student");
    }

    private static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            Configuration configuration = new Configuration();
            configuration.configure();
            ServiceRegistry serviceRegistry = new ServiceRegistryBuilder()
                    .applySettings(configuration.getProperties())
                    .buildServiceRegistry();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        }
        return sessionFactory;
    }
}
