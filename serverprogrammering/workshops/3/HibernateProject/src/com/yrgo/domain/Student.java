package com.yrgo.domain;

import javax.persistence.*;

@Entity
@Table(name="TBL_STUDENT")
public class Student
{
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String enrollmentID;
    private String name;
    private String tutorName; // This will become a class soon
    private Integer numberOfCourses;

    public Student(){}

    public Student(String name){
        this.name = name;
        this.numberOfCourses = 10;
    }


    public Student(String name, String tutorName) {
        this.name = name;
        this.tutorName = tutorName;
        this.numberOfCourses = 10;
    }


    @Id
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }


    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", enrollmentID='" + enrollmentID + '\'' +
                ", name='" + name + '\'' +
                ", tutorName='" + tutorName + '\'' +
                '}';
    }
}

