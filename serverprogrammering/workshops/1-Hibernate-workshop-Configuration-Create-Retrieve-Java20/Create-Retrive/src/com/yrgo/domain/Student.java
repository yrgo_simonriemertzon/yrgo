package com.yrgo.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String enrollmentID;
    private String name;
    private String tutorName; // This will become a class soon

    private String getEnrollmentID() {
        return enrollmentID;
    }

    private void setEnrollmentID(String enrollmentID) {
        this.enrollmentID = enrollmentID;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    public String getTutorName() {
        return tutorName;
    }

    public void setTutorName(String tutorName) {
        this.tutorName = tutorName;
    }

    public Student() {

    }

    public static Student getNewStudent(String name, String tutorName) {
        return new Student(name, tutorName);
    }

    private Student(String name, String tutorName) {
        this.name = name;
        this.tutorName = tutorName;
    }

    public Student(String name) {
        this.name = name;
        this.tutorName = null;
    }

    @Override
    public String toString() {
        return "Student{" +
                "enrollmentID='" + enrollmentID + '\'' +
                ", name='" + name + '\'' +
                ", tutorName='" + tutorName + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void updateStudentWith(Student updatedStudent) {
        this.name = updatedStudent.name;
    }


}
