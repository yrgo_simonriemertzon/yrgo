package com.yrgo.test;

import com.yrgo.domain.Student;
import com.yrgo.domain.Tutor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import java.util.Set;

public class HibernateTest {
    private static SessionFactory sessionFactory = null;
    private static Session session;
    private static Transaction tx;

    private static void initiateSession() {
        SessionFactory sf = getSessionFactory();
        session = sf.openSession();
    }

    private static void closeSession() {
        session.close();
    }

    public static void main(String[] args) {
        initiateSession();
        initiateDB();
        addStudentGroup();
      /*  getStudent(1);
        getStudentAndTutorAndPrintThem();
        printTutorNameFromStudent(1);*/
        closeSession();
    }

    private static void addStudentGroup() {
        tx = session.beginTransaction();
        Tutor newTutor = new Tutor("ABC234", "Natalie Woodward", 387787);
        Student student1 = new Student("Patrik Howard");
        Student student2 = new Student("Marie Sani");
        Student student3 = new Student("Tom Nikson");

        newTutor.addStudentToTeachingGroup(student1);
        newTutor.addStudentToTeachingGroup(student2);
        newTutor.addStudentToTeachingGroup(student3);
        System.out.println("HELLO HELLO HELLO!!!!");
        Set<Student> students = newTutor.getTeachingGroup();
        for(Student student: students) {
            System.out.println(student);
        }

        session.save(student1);
        session.save(student2);
        session.save(student3);
        session.save(newTutor);

       /* Tutor myTutor = (Tutor)session.get(Tutor.class, 2);
        Set<Student>students = myTutor.getTeachingGroup();
        for(Student s: students) {
            System.out.println(s);
        }

        Student student4 = new Student("Julia Ericcson");
        session.save(student4);
        myTutor.addStudentToTeachingGroup(student4);*/

        tx.commit();
    }

    private static void initiateDB() {
        tx = session.beginTransaction();
        Tutor newTutor = new Tutor("ABC 126 Beep beep I'm a Jeep", "Bosse");
        Student newStudent = new Student("Annabelle");
        session.save(newStudent);
        session.save(newTutor);
        tx.commit();
    }

    private static void printTutorNameFromStudent(int id) {
        Student myStudent = (Student) session.get(Student.class, id);
        //Tutor myTutor = myStudent.getTutor(); // TODO: 15-Apr-21 This must not return null
        //System.out.println("This is my tutors name: " + myTutor.getName());
    }

    private static void getStudent(int id) {
        Student myStudent = (Student) session.get(Student.class, id);
        System.out.println(myStudent + " is the student");
    }

    private static void getStudentAndTutorAndPrintThem() {
        SessionFactory sf = getSessionFactory();
        Session session = sf.openSession();
        Student myStudent = (Student) session.get(Student.class, 1);
        Tutor myTutor = (Tutor) session.get(Tutor.class, 1);
        System.out.println(myStudent);
        System.out.println(myTutor);
    }

    private static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            Configuration configuration = new Configuration();
            configuration.configure();
            ServiceRegistry serviceRegistry = new ServiceRegistryBuilder()
                    .applySettings(configuration.getProperties())
                    .buildServiceRegistry();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        }
        return sessionFactory;
    }
}
