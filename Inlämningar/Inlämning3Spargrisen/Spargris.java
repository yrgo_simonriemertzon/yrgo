public class Spargris {
    private int coins;
    private int valueOfCoins;

    public Spargris(int coins) {
        this.coins = coins;
    }

    public void addCoin(int coin) {
        if (coin == 1 || coin == 2 || coin == 5 || coin == 10) {
            this.coins++;
            valueOfCoins += coin;
            
        } 
    }

    public int getNumberOfCoins() {
        return this.coins;
    }

    public int getValueOfCoins() {
        return this.valueOfCoins;
    }
}