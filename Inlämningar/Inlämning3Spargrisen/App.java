import java.util.Scanner;
public class App {
    public static void main(String[] args) {
        Spargris gris = new Spargris(0);
        Scanner scan = new Scanner(System.in);

        int coin;
        do {
            System.out.println("Vilket mynt vill du spara?");
            coin = scan.nextInt();
            gris.addCoin(coin);
        } while (coin != 0);

        System.out.printf("Du har sparat %d mynt till ett totalt värde av %d kronor", gris.getNumberOfCoins(),
                gris.getValueOfCoins());

        scan.close();
    }
}
