package com.yrgo.siniom;

public class InsufficientBalanceException extends Exception {
	
	public InsufficientBalanceException(String message ) {
		super(message);
	}
}
