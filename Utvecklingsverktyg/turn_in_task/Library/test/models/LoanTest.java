package models;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import utilities.GenderType;

import java.time.LocalDate;

public class LoanTest {

    //VG
    @Test
    public void testDueDate() {
        Book book1 = new Book(0,
                "Moby Dick",
                "Charles Dickens",
                "10203040",
                "Western", 340);
        Customer customer1 = new Customer("Mr",
                "Simon",
                "Smith",
                "Supergatan",
                "28283939200",
                "simon@gmail.com ",
                10,
                GenderType.MALE);

        Loan testLoan = new Loan(100, customer1, book1);
    assertEquals(testLoan.getDueDate(), LocalDate.now().plusDays(14));
    }
}