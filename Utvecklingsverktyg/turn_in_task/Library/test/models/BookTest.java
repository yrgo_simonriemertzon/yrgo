package models;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class BookTest {

    //G
    @Test
    public void test2EqualBooks() {
        //1. The only thing that diffrenciate one book from another is the id!
        Book book1 = new Book(0,
                "Moby Dick",
                "Charles Dickens",
                "10203040",
                "Western", 340);
        Book book2 = new Book(0,
                "Moby Flick", //2. That means a title can be different without the test failing
                "Charles Dickens",
                "10203040",
                "Western", 340);
        assertEquals(book1, book2);
    }

    //G
    @Test
    public void test2NonEqualBooks() {
        //The only thing that diffrenciate one book from another is the id!
        Book book1 = new Book(0,
                "Moby Dick",
                "Charles Dickens",
                "10203040",
                "Western", 340);
        Book book2 = new Book(2,
                "Moby Dick",
                "Charles Dickens",
                "10203040",
                "Western", 340);
        assertNotEquals(book1, book2);
    }
}
