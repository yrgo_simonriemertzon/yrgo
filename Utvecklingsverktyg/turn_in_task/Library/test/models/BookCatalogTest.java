package models;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class BookCatalogTest {

	private BookCatalog bc;
	private Book book1;

	public BookCatalogTest() {
		bc = new BookCatalog();
		Book book1 = new Book(1,"Learning Java","","","",0);
		bc.addBook(book1);
	}

	//G
	@Test
	public void testAddABook() {
		var numberOfBooksBeforeAdding = bc.getNumberOfBooks();
		Book book2 = new Book(2,
				"Moby Dick",
				"Charles Dickens",
				"10203040",
				"Western", 340);
		bc.addBook(new Book(1,"Learning Java","","","",0));
		bc.addBook(book2);
		var numberOfBooksAfterAdding = bc.getNumberOfBooks();

		assertEquals(numberOfBooksBeforeAdding + 1, numberOfBooksAfterAdding);

	}

	//G
	@Test
	public void testFindBook() throws BookNotFoundException {
		var foundBook = bc.findBook("Learning Java");
		var foundBookAuthor = foundBook.getAuthor();
		assertEquals(foundBookAuthor, "");
	}

	//G
	@Test
	public void testFindBookIgnoringCase() throws BookNotFoundException {
		var foundBook = bc.findBook("LEARNING Java");
		var foundBookAuthor = foundBook.getAuthor();
		assertEquals(foundBookAuthor, "");
	}

	//G
	@Test
	public void testFindBookWithExtraSpaces() throws BookNotFoundException {
		//This test only takes into account spaces before or after the title! NOT if there is extra space between words.
		//See next test for a demonstration!
		var foundBook = bc.findBook("Learning Java       ");
		var foundBookAuthor = foundBook.getAuthor();
		assertEquals(foundBookAuthor, "");
	}

	//VG
	// This test should throw BookNotFoundException in order to pass.
	@Test
	public void testFindBookThatDoesntExist() throws BookNotFoundException {
		//This test only takes into account spaces before or after the title! NOT if there is extra space between words.
		assertThrows(BookNotFoundException.class, () -> bc.findBook("Learning         Java"));
	}

}
