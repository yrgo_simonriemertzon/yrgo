package com.yrgo.isbnvalidator;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StockSystemTest {

    @Test
    public void testGetValidIndicator() {
        StockSystem stockSystem = new StockSystem();
        String isbn = "069117654X";
        String indicator = stockSystem.getIndicator(isbn);
        assertEquals("654XB4", indicator);  //expected = "654XB4"
    }


}
