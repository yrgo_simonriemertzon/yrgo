package com.yrgo.isbnvalidator;

import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

class ValidateISBNTest {
    ValidateISBN validator;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        validator = new ValidateISBN();
    }

    @Test
    public void checkValidISBN() {
        boolean result = validator.checkISBN("0140441492");
        assertTrue(result, "First Value");
        result = validator.checkISBN("0596009658");
        assertTrue(result, "Second Value");
    }

    @Test
    public void checkInvalidISBN() {
        boolean result = validator.checkISBN("0140441493");
        assertFalse(result);
    }

    @Test
    public void nineDigitNotAllowed() {
        assertThrows(NumberFormatException.class, () ->
        {
            validator.checkISBN("123456789");
        });
    }

    @Test
    public void nonDigitsNotAllowed() {
        assertThrows(NumberFormatException.class, () -> {
         validator.checkISBN("738SBNNumb") ;
        });
    }

    @Test
    public void ISBNthatEndWithXIsValid () {
        boolean result = validator.checkISBN("069117654X");
        assertTrue(result);
    }

    @Test
    public void checkValid13DigitISBN() {
        ValidateISBN validator = new ValidateISBN();
        boolean result = validator.checkISBN("9780691176543");
        assertTrue(result, "First value");
        result = validator.checkISBN("9780596009656");
        assertTrue(result, "Second value");
    }





}