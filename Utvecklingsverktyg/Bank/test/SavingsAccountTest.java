import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
public class SavingsAccountTest {
    SavingsAccount account;

    @BeforeEach
    private void setup() {
        account = new SavingsAccount(0);
    }

    @Test
    void createAccountWithInitialBalance() {
        SavingsAccount account = new SavingsAccount(100);
        assertEquals(100, account.balance(), 0.001);
    }

    @Test
    void createInvalidAccountWithNegativeBalance() {
        assertThrows(NegativeAmountException.class, () -> new SavingsAccount(-100));
    }

    @Test
    void checkValidDeposit() {
        assertEquals(0, account.balance(), 0.001);
        account.deposit(100);
        assertEquals(100, account.balance(), 0.001);
    }

    @Test
    void checkNegativeAmountDeposit() {
        assertThrows(NegativeAmountException.class, () -> account.deposit(-1));
    }

    @Test
    void allowUserToWithdrawFunds() {
        account.deposit(100);
        account.withdraw(50);
        assertEquals(50, account.balance(), 0.0001);
    }

    @Test
    void denyUserToWithdrawNegativeAmount () {
        account.deposit(100);
        assertThrows(NegativeAmountException.class, () -> account.withdraw(-100));
    }

    @Test
    void denyUserToWithdrawMoreThanBalance(){
        account.deposit(100);
        assertThrows(InsufficientBalanceException.class, () -> account.withdraw(101));
    }






}
