import javax.naming.InsufficientResourcesException;
import java.util.ArrayList;

public class SavingsAccount {
    private double balance;

    public double balance() {
        return balance;
    }

    public SavingsAccount(double initialBalance) {
        if (initialBalance < 0) {throw new NegativeAmountException("Account cant have negative initial value");}
        this.balance = initialBalance;
    }

    public void deposit(double amount) {
        if (amount < 0) { throw new NegativeAmountException("Deposit amount cant have a negative value");}
        balance += amount;
    }

    public void withdraw(double amount) {
        if (amount < 0) { throw new NegativeAmountException("Withdraw amount cant have a negative value");}
        if (amount > balance) { throw new InsufficientBalanceException("Withdraw amount cant be bigger than available balance");}
        this.balance -= amount;
    }
}
