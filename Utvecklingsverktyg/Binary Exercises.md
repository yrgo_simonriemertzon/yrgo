# [Binary Exercises](http://wiki.juneday.se/mediawiki/index.php/ITIC:Digital_representation_-_Binary_-_Exercises)

## 01a

2^9 = 512

## 01b

0-512

## 03

| 128 | 64 | 32| 16 | 8 | 4 | 2 | 1 | **SUM** in decimal | **SUM** in hex |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| 1 | 1 | 1 | 1 | 1 | 1 | 1 | 1 |  255|
| 1 | 1 | 1 | 1 | 1 | 1 | 1 | 1 |  255| B1

___

# Hexadecimals
0123456789
- A = 10
- B = 11
- C = 12
- D = 13
- E = 14
- F = 15

| 1024 | 512 | 256 | 128 | 64 | 32 | 16 | 1 | **SUM** in hex |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---:
|  |  |  |  |  |  | B | 1 | 177 |
|  |  |  |  |  |  |  | A | 10 |
|  |  |  |  |  |  |  | F | 15 |
|  |  |  |  |  |  | 1 | 2 | 18 |
|  |  |  |  |  |  | 1 | A | 26 |
|  |  |  |  |  |  | 2 | 0 | 32 |

1. 1001 in binary. = 9 in decimal(bas10) = 9 in hex- (bas16)
2. 0101 in binary. = 5 in decimal(bas10) = 5 in hex - (bas16)
3. 0011 in binary  = 3 in decimal(bas10) = 3 in hex - (bas16)
4. 0010 in binary  = 2 in decimal(bas10) = 2 in hex - (bas16)
5. 1111 in binary  = 15 in decimal(bas10) = F in hex - (bas16)
