package se.yrgo.riemertzon;

public class SavingsAccount{
  private double balance;
  public SavingsAccount(){

  }
  public SavingsAccount(double initialBalance){
    this.balance = initialBalance;
  }

  
  
  
  
  
  
  
  
  public void withdraw(double amount) throws InsufficientBalanceException{
    if( (balance-amount) >=0.0 ){
      balance -= amount;
    }else{   	
    	throw new InsufficientBalanceException(String.format("Not enough funds. Balance: %s, "
				+ "Amount Requested %s", balance, amount));
    }
  }
  
  
  
  
  
  
  
  
  
  public void deposit(double amount){
    balance += amount;
  }
  
  public double balance(){
    return balance;
  }
  
  @Override
  public String toString(){
    return String.format("Balance: %.2f", balance);
  }
}
