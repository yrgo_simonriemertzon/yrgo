package se.yrgo.riemertzon.test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import se.yrgo.riemertzon.InsufficientBalanceException;
import se.yrgo.riemertzon.SavingsAccount;

import org.junit.jupiter.api.Test;

public class SavingsAccountTest {
	private SavingsAccount accountWithHundred;
	
	@BeforeEach
	void initEach() {
		accountWithHundred = new SavingsAccount(100);
	}
	
	@Test
	void withdrawTooMuch() {
		Double tooMuch = 220d;
		try {
			accountWithHundred.withdraw(tooMuch);
			fail("Exception was NOT thrown!");
		} catch (InsufficientBalanceException e) {
			System.out.println("Exception sucsessfully thrown! Exception:" +
								e.getMessage());
		}
	}
	
	@Test
	void withdrawAll() {
		Double aHundred = 100.0;
		try {
			accountWithHundred.withdraw(aHundred);
		} catch (InsufficientBalanceException e) {
			fail("Unexpected exception thrown");
			System.out.println(" Exception:" +
								e.getMessage());
		}
		assertEquals(0, accountWithHundred.balance());
	}
	

}
