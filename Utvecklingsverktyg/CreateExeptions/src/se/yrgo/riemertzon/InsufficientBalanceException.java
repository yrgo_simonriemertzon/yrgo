package se.yrgo.riemertzon;

public class InsufficientBalanceException extends Exception {
	
	public InsufficientBalanceException(String message) {
		super(message);
	}
	
	
	
}
