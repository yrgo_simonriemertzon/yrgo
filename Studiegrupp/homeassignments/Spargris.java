public class Spargris {
    private int numberOfCoins;
    private int sumCoins;

    public int getNrOfCoins() {
        return numberOfCoins;
    }

    public int getSumCoins() {
        return sumCoins;
    }
    
    public void saveCoin(int valueOfCoin) {
        if(valueOfCoin == 1 || valueOfCoin == 2 || valueOfCoin == 5 || valueOfCoin == 10) {
            numberOfCoins++;
            sumCoins += valueOfCoin;
        } 
    }

}
