--**------**------**------**------**------**------**------**------**------**------**------**------**------**------**----
--Creation
CREATE TABLE Books
(
    title VARCHAR(70) NOT NULL,
    author VARCHAR(50) NOT NULL,
    isbn VARCHAR(17) NOT NULL PRIMARY KEY,
    classification SMALLINT NOT NULL,
    library_aquired DATE,
    stock_aquired INT IDENTITY(1,1),
​
)
​
CREATE TABLE Customers
(
    id INT IDENTITY(1,1) PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    email VARCHAR(70),
    address VARCHAR(100) NOT NULL
​
)
​
CREATE TABLE Borrowed
(
    customer_id INT FOREIGN KEY(customer_id) REFERENCES Customers(id),
    isbn VARCHAR(17) FOREIGN KEY(isbn) REFERENCES Books(isbn),
    return_date DATE
)
​
--**------**------**------**------**------**------**------**------**------**------**------**------**------**------**----
--Insertion
INSERT INTO Books
    (title, author, isbn, classification, library_aquired)
VALUES
    ('L''oeuvre intégrale Jacques Brel', 'Jacques Brel', '0-9139-6816-1', 920, '2013-07-11'),
    ('Bara Ben', 'Kjell Brell', '0-2707-0383-7', 782, '2013-07-11'),
    ('Bockarna Bruse', 'Kjell Brell', '978-3-16-148410-0', 782, '2010-05-23')
​
INSERT INTO Customers
    ( name, email, address)
VALUES
    ('Simon Riemertzon', 'simon.riemertzon@gmail.com', 'Medlevägen 85'),
    ('Kenta Kentsson', 'asd@gmail.com', 'Sotaregatan 2'),
    ('Fernando Vigall', 'krulltott@gmail.com', 'Sotaregatan 4')
​
--**------**------**------**------**------**------**------**------**------**------**------**------**------**------**----
--Here we delete rows
DELETE FROM Books WHERE isbn LIKE '0-9139-6816-1'