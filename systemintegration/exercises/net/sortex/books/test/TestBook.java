package net.sortex.books.test;
import java.util.Arrays;
import net.sortex.books.Book;

public class TestBook {

    public static void main(String[] args) {
        Book b1 = new Book("Simon");
        Book b2 = new Book("Anna");
        Book b3 = new Book("Erik");
        Book b4 = new Book("Simon");

        Book[] books = {b1, b2, b3, b4};

        Arrays.sort(books);
        
        Arrays.stream(books).foreach( b -> System.out.println(b.getName()));
        
    }   
}
