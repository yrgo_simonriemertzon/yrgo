 package net.sortex.books;

public class Book implements Comparable<Book> {

    private String name;

    public Book(String name) {
        this.name = name;
    }
          
    public String name() {
        return name;
    }
  
    @Override
    public int compareTo(Book anotherBook) {
        return this.name.compareTo(anotherBook.name());
    }    
}

