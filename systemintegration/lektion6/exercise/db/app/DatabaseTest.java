package db.app;

import java.sql.*;

public class DatabaseTest {
  private static Connection con;
  private final static String DB_CONN_STR = "jdbc:sclite:my_municipalities";
  static {
    try {
      Class.forName("org.sqlite.JDBC");
    } catch (ClassnotFoundException cnfe) {
      System.err.println("Could not load jdbc driver : " + cnfe.getMessage());
    }
  }

  public DatabaseTest() {
    getConnection();
  }

  private getConnection() {
    try {
      con = DriverManager.getConnection(DB_CONN_STR);
    } catch (Exception e) {
      System.err.println("Error getting connection to : " + e.getMessage());
    }
      
  }

  public hasConnection() {
    return con != null;
  }

  public makeQueary() {
    Statement statement = null;
    ResultSet result = null;
    
    try {
      statement = con.createStatement();
      
      String query = "SELECT Name, HTTPS FROM municipalities LIMIT 5";
      statement.executeQuery(query);

      while (result.next()) {
        System.out.println(rs.getString("Name") + " " + (rs.getBoolean("HTTPS") ? "HTTPS support" : "HTTP only"));
      }
    } catch (SQLException e) {
      System.err.println("Error when using query", e.getMessage());
      //TODO: handle exception
    } finally {
      try {
        rs.close();
          statement.close();
      } catch (SQLException)

    }
  }

}
