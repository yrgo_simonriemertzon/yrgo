package db.main;
import db.app.DatabaseTest;
public class Main {
   
    public static void main(String[] args){
       System.out.println("Running the class db.main.Main.");
       DatabaseTest dt = new DatabaseTest();
       if(dt.hasConnection()) {
           System.out.println("Connection established");
       } else {
           System.out.println("Connection could not be established");
       }
       dt.testQuery();
    }

}
